-module('9p_zero').

-export([is_local/1]).
-export([connect/1,connect/2]).
-export([listen/1]).
-export([accept/2,accept/3]).
-export([send/3]).
-export([recv/2,recv/3]).
-export([set_maximum_size/3]).
-export([activate/2]).
-export([close/2]).

-include("9p.hrl").

is_local(_) -> true.

connect(TransConf) ->
	connect(TransConf, infinity).

connect(Realm, Timeout) when is_atom(Realm) ->
	%erlang:display({'9p_zero',connect,Realm}),
	Realm ! {connect,self()},
	receive
	{connected,Peer} ->
		{ok,Peer}
	after Timeout ->
		{error,timeout}
	end.

listen(Realm) when is_atom(Realm) ->
	%erlang:display({'9p_zero',listen,Realm}),
	Pid = spawn_link(fun() -> loop({[],[]}) end),
	register(Realm, Pid),
	{ok,Realm}.
	
accept(Realm, TransConf) ->
	accept(Realm, TransConf, infinity).

accept(Realm, _TransConf, Timeout) ->
	%erlang:display({'9p_zero',accept,Realm}),
	Realm ! {accept,self()},
	receive
	{accepted,Peer} ->
		{ok,Peer}
	after Timeout ->
		{error,timeout}
	end.

send(Peer, Msg, _TransConf) ->
	%erlang:display({'9p_zero',send,Peer,Msg}),
	Peer ! {'9p_zero',self(),Msg},
	ok.

recv(Peer, TransConf) ->
	recv(Peer, TransConf, infinity).

recv(Peer, _TransConf, Timeout) ->
	%erlang:display({'9p_zero',recv,Peer}),
	receive
	{'9p_zero',Peer,Msg} ->
		{ok,Msg}
	after Timeout ->
		{error,timeout}
	end.

set_maximum_size(_Peer, _MSize, _TransConf) ->
	ok.

activate(_Peer, _TransConf) ->
	{ok,{'9p_zero',undefined,undefined}}.

close(Realm, _TransConf) when is_atom(Realm) -> %% "listening socket"
	case whereis(Realm) of
	undefined ->
		ok;
	Pid ->
		exit(Pid, normal)
	end;
close(_Peer, _TransConf) ->
	ok.

%%------------------------------------------------------------------------------

loop({Connecting,Accepting}) ->
	receive
	{connect,Pid} when Accepting =:= [] ->
		loop({Connecting ++ [Pid],Accepting});

	{connect,Pid} ->
		Peer = hd(Accepting),
		Peer ! {accepted,Pid},
		Pid ! {connected,Peer},
		loop({Connecting,tl(Accepting)});

	{accept,Pid} when Connecting =:= [] ->
		loop({Connecting,Accepting ++ [Pid]});

	{accept,Pid} ->
		Peer = hd(Connecting),
		Pid ! {accepted,Peer},
		Peer ! {connected,Pid},
		loop({tl(Connecting),Accepting})

	end.

%%EOF
