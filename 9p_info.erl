%%%
%%% @doc Accessors for 9p connection parameters. All functions of a callback
%%% module that implements an exported directory take and opaque `Conn' parameter.
%%% The '9p_info':* functions provide access to various bits of information stored
%%% in the `Conn' structure. The primary purpose is to make informed decisions
%%% about permissions of files and directories.
%%%
-module('9p_info').
-export([version/1]).
-export([peer_node/1,peer_group/1]).
-export([auth_path/1,auth_user/1,auth_user_id/1]).
-export([unix_user/1,unix_group/1]).

-include("9p_info.hrl").

-type opaque() :: any().

%% @doc Returns the negotiated version of the 9p protocol. '9P2000.e' is Erlang
%% extension version used by Erlang on Xen nodes. Linux clients based on v9fs
%% kernel module use '9P2000.u' version.
-spec version(Conn) -> '9P2000.e' | '9P2000.u' when
	Conn :: opaque().

version(#ac{ver =e}) -> '9P2000.e';
version(#ac{ver =u}) -> '9P2000.u'.

%% @doc Returns the node id of the connected peer discovered using the MUMBLE
%% authentication exchange.
-spec peer_node(Conn) -> Node | undefined when
	Conn :: opaque(),
	Node :: atom().

peer_node(#ac{peer_node =N}) -> N.

%% @doc Returns the node group of the connected peer discovered using the MUMBLE
%% authentication exchange.
-spec peer_group(Conn) -> Group | undefined when
	Conn :: opaque(),
	Group :: atom().

peer_group(#ac{peer_group =G}) -> G.

%% @doc Returns the path (aname) indicate in the auth message.
-spec auth_path(Conn) -> Path | undefined when
	Conn :: opaque(),
	Path :: binary().

auth_path(#ac{auth_path =AP}) -> AP.

%% @doc Returns the user name indicated in the auth message.
-spec auth_user(Conn) -> User | undefined when
	Conn :: opaque(),
	User :: binary().

auth_user(#ac{auth_user =AU}) -> AU.

%% @doc Returns the numeric used id that may be indicated in the auth message
%% ('9P2000.u' only).
-spec auth_user_id(Conn) -> UserId | undefined when
	Conn :: opaque(),
	UserId :: integer().

auth_user_id(#ac{auth_uid =ID}) -> ID.

%% @doc Returns the Unix user id of the connected peer discovered during a
%% successful MUNGE authentication exchange.
-spec unix_user(Conn) -> UserId | undefined when
	Conn :: opaque(),
	UserId :: integer().

unix_user(#ac{unix_user =UU}) -> UU.

%% @doc Returns the Unix group id of the connected peer discovered during a
%% successful MUNGE authentication exchange.
-spec unix_group(Conn) -> GroupId | undefined when
	Conn :: opaque(),
	GroupId :: integer().

unix_group(#ac{unix_group =UG}) -> UG.

%%EOF
