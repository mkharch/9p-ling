-module('9p_tcp').

-export([is_local/1]).
-export([connect/1,connect/2]).
-export([listen/1]).
-export([accept/2,accept/3]).
-export([send/3]).
-export([recv/2,recv/3]).
-export([set_maximum_size/3]).
-export([activate/2]).
-export([close/2]).

-include("9p.hrl").

-define(CONCURRENCY, 1).

is_local({localhost,_}) -> true;
is_local({{127,0,0,_},_}) -> true; %% IPv6?
is_local(_) -> false.

connect(TransConf) ->
	connect(TransConf, infinity).

connect({Host,Port}, Timeout) ->
	gen_tcp:connect(Host, Port, [{active,false},
								 {mode,binary},
								 {recbuf,?MSIZE},
								 {packet,'9p'}], Timeout);
connect(_, _) ->
	{error,einval}.

listen({Addr,Port}) when is_integer(Port) ->
	gen_tcp:listen(Port, [{ip,Addr},
						  {active,false},
					 	  {mode,binary},
						  {packet,'9p'}]).

accept(Sock, TransConf) ->
	accept(Sock, TransConf, infinity).

accept(ListenSock, _TransConf, Timeout) ->
	case gen_tcp:accept(ListenSock, Timeout) of
	{ok,Sock} =Res ->

		%%
		%% 9p_mounter multiplexes many file operations over a single
		%% connection. Many such connections may be active. Set the size of the
		%% receive buffer high to prevent overlflowing.
		%%

		inet:setopts(Sock, [{recbuf,?MSIZE *?CONCURRENCY}]),
		Res;
	Other ->
		Other
	end.

send(Sock, Msg, _TransConf) when is_tuple(Msg) ->
	gen_tcp:send(Sock, '9p':encode(Msg)).

recv(Sock, TransConf) ->
	recv(Sock, TransConf, infinity).

recv(Sock, _TransConf, Timeout) ->
	gen_tcp:recv(Sock, 0, Timeout).

set_maximum_size(Sock, MSize, _TransConf) ->
	%% NB: recbuf not set -- MSize may be 4Gb
	inet:setopts(Sock, [{packet_size,MSize}]).

activate(Sock, _TransConf) ->
	inet:setopts(Sock, [{active,true}]),
	{ok,{tcp,tcp_closed,tcp_error}}.

close(Sock, _TransConf) ->
	gen_tcp:close(Sock).

%%EOF
