%%%
%%% @doc A data blob bucket export module. The module implements callbacks needed
%%% for exporting a single bucket of named data blobs. The module functions are
%%% only called indirectly by '9p_server'.
%%%
%%% ModConf is an atom, the bucket name.
%%%
%%% @end
%%% 

-module(embedded_export).

-include("9p.hrl").

%% Callbacks
-export([list_dir/2,find/3]).
-export([read/5]).
-export([truncate/4]).
-export([top_stat/2,file_stat/3]).

%% -spec top_granted(binary() | {binary(),integer()} | undefined, any(), any())
%% -> boolean().
%%
%% top_granted(User, Conn, Bucket) -> true.
%%
%% -spec file_granted(binary(), binary() | {binary(),integer()} | undefined,
%% any(), any()) -> boolean().
%%
%% file_granted(File, User, Conn, Bucket) -> true.

-spec list_dir(any(), any()) -> [{binary(),any()}].

list_dir(_Conn, Bucket) when is_atom(Bucket) ->
	AFiles = binary:list_embedded(Bucket),
	[{to_bin(A),A} || A <- AFiles].

-spec find(binary(), any(), any()) -> {found,_} | false.

find(File, _Conn, Bucket) when is_atom(Bucket) ->
	AName = to_atom(File),
	case binary:embedded_size(Bucket, AName) of
	false ->
		false;
	_Size ->
		{found,AName}
	end.

%% -spec create(binary(), any(), any()) -> boolean().
%%
%% create(Name, Conn, ModConf) -> false.

%% -spec remove(any(), any(), any()) -> boolean().
%%
%% remove(Name, Conn, ModConf) -> false.

-spec read(any(), integer(), integer(), any(), any()) -> binary().

read(File, Offset, Count, _Conn, Bucket) when is_atom(Bucket) ->
	case binary:embedded_size(Bucket, to_atom(File)) of
	false ->
		false;
	Size ->
		N = if Offset +Count > Size -> Size -Offset;
			true -> Count end,
		binary:embedded_part(Bucket, to_atom(File), Offset, N)
	end.

%% -spec write(any(), integer(), binary(), any(), any()) -> integer().
%% 
%% write(File, Offset, Data, Conn, Bucket) when is_atom(Bucket) -> 0.

-spec truncate(any(), integer(), any(), any()) -> integer().

truncate(File, _Size, _Conn, Bucket) when is_atom(Bucket) ->
	binary:embedded_size(Bucket, to_atom(File)). %% no truncation

-spec top_stat(any(), any()) -> #stat{}.

top_stat(_Conn, Bucket) when is_atom(Bucket) ->
	#stat{name =to_bin(Bucket),length =0}.

-spec file_stat(any(), any(), any()) -> #stat{}.

file_stat(File, _Conn, Bucket) when is_atom(Bucket) ->
	Length = binary:embedded_size(Bucket, to_atom(File)),
	#stat{name =to_bin(File),length =Length}.

%%------------------------------------------------------------------------------

to_bin(A) when is_atom(A) ->
	list_to_binary(atom_to_list(A));
to_bin(B) ->
	B.

to_atom(B) when is_binary(B) ->
	list_to_atom(binary_to_list(B));
to_atom(A) ->
	A.

%%EOF
